---
title: Tareas
subtitle: Redes 2020-2
date: 2020-01-27
tags: ["tareas"]
---

En este espacio estaremos concentrando las tareas de la materia de [redes de computadoras][gitlab-redes] semestre 2020-2 impartida en la [Facultad de Ciencias, UNAM][fciencias-unam].

[gitlab-redes]: http://Redes-Ciencias-UNAM.gitlab.io/
[fciencias-unam]: http://www.fciencias.unam.mx/docencia/horarios/20202/1556/714

